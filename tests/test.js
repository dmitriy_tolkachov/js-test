/**
 * Created by Dmitriy T on 11.10.2014.
 */

var _passedTestsCnt = 0;
var _failedTestsCnt = 0;
var _notCompletedTests = 0;
var outputTestResult = function(name,result){
    var resVerb = result?"Passed":"Failed";
    //var tmpl = "<tr class='"+resVerb+"'><td>"+name+"</td><td>"+resVerb+"</td></tr>";
    var child = document.createElement('tr');
    child.setAttribute('class',resVerb.toLowerCase());
    var td1 = document.createElement('td');
    td1.innerHTML = name;
    child.appendChild(td1);
    var td2 = document.createElement('td');
    td2.innerHTML = resVerb;
    child.appendChild(td2);

    document.getElementById("tests-list").appendChild(child);

    if(result){
        _passedTestsCnt +=1;
    }
    else{
        _failedTestsCnt +=1;
    }
};
var outputTestSummary = function(){
    document.getElementById("tests-cnt").innerHTML = appTest.tests.length;
    document.getElementById("tests-passed-cnt").innerHTML = _passedTestsCnt;
    document.getElementById("tests-failed-cnt").innerHTML = _failedTestsCnt;
    document.getElementById("tests-not-completed-cnt").innerHTML = appTest.tests.length - _failedTestsCnt - _passedTestsCnt;
}

var appTest = {};

//tests:
appTest.tests=[
    //test: creation of Collection
    function test1(){
        this.name = "creation of Collection";
        try{
            var collection = new Collection();
            if(collection){
                //TODO: add additional checking it object is Collection
                outputTestResult(this.name,true);
            }
            else{
                outputTestResult(this.name,false);
            }
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: add item to Collection
    function test2(){
        this.name = "add item to Collection";
        try{
            var collection = new Collection();
            var initCnt = collection.mItemsCount();

            collection.mAddItem(new Item("Carrots",collection));
            var afterAddCnt = collection.mItemsCount();

            outputTestResult(this.name,(initCnt==0 && afterAddCnt==1));
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: check Collection length
    function test3(){
        this.name = "check Collection length";
        try{
            var collection = new Collection();
            collection.mAddItem(new Item("Carrots",collection));
            var cnt = collection.mItemsCount();

            outputTestResult(this.name,(cnt==1));
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: remove item from Collection
    function test4(){
        this.name = "remove item from Collection";
        try{
            var collection = new Collection();
            var item = new Item("Carrots",collection);

            collection.mAddItem(item);
            var cntAfterAdd = collection.mItemsCount();

            collection.mRemoveItem(item);
            var cntAfterRemove = collection.mItemsCount();

            outputTestResult(this.name,(cntAfterAdd==1 && cntAfterRemove==0));
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: creation of Item
    function test5(){
        this.name = "creation of Item";
        try{
            var item = new Item();
            //TODO: add additional checking if object is Item
            //if object created -> test passed
            if(item){
                outputTestResult(this.name,true);
            }
            else{
                outputTestResult(this.name,false);
            }
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: validate valid Item
    function test6(){
        this.name = "validate valid Item";
        try{
            var item = new Item('value 1');
            //if item has value -> it is valid
            if(item.mIsValid()){
                outputTestResult(this.name,true);
            }
            else{
                outputTestResult(this.name,false);
            }
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: validate not valid Item
    function test7(){
        this.name = "validate not valid Item";
        try{
            var item = new Item('');
            //if item has no value -> it is not valid
            if(!item.mIsValid()){
                outputTestResult(this.name,true);
            }
            else{
                outputTestResult(this.name,false);
            }
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    },

    //test: remove Item (no collection)
    function test8(){
        this.name = "remove Item (no collection)";
        try{
            var item = new Item();
            //you can`t remove item that has no collection reference
            if(item.mRemove()){
                outputTestResult(this.name,false);
            }
            else{
                outputTestResult(this.name,true);
            }
        }
        catch (e){
            outputTestResult(this.name,false);
        }
    }
];

//run tests
for (var i in appTest.tests) {
    appTest.tests[i]();
}
outputTestSummary();