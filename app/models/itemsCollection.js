/**
 * Created by Dmitriy T on 11.10.2014.
 */

var Collection = function(){
    //private

    //properties
    this._items = []; //items stores here

    //methods
    //return items collection length
    this._getItemsCount = function(){
        //TODO: use private property (change context)
        return this.items.length;
    };
    //add item to collection
    this._addItem = function(item){
        //validate item before adding to collection
        //TODO: do not allow add items with the same value (until value is unique property)
        //TODO: check if item is Item class instance
        if(item && item.mIsValid()){
            //TODO: use private property (change context)
            item.collection = this; //set reference to this collection
            this.items.push(item);
            return item;
        }
        console.log('Can`t add item to collection: item is not valid.')
        return null;
    };
    //remove item from collection
    this._removeItem = function(item){
        //validate input
        if(!(item && item.mIsValid())){
            console.log("Can`t remove item: item to delete is not valid.");
            return null;
        }

        //loop through collection
        //find item`s index
        //remove item by index
        var cnt = this.mItemsCount(), indexToRemove = null;
        for(var i=0;i<cnt;i++){
            var collectionItem = this.items[i];
            if(collectionItem.value.toLowerCase() === item.value.toLowerCase()){
                indexToRemove = i;
                break;
            }
        }

        if(indexToRemove!=null){
            this.items.splice(indexToRemove, 1);
            return this.items;
        }

        console.log("Can`t remove item: can`t find such item in the collection.");
        return null;
    };

    //public
    return {
        items: this._items,
        mAddItem: this._addItem,
        mRemoveItem: this._removeItem,
        mItemsCount: this._getItemsCount
    }
}