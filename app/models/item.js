/**
 * Created by Dmitriy T on 11.10.2014.
 */

var Item = function(value,collection){
    //private

    //properties
    //TODO: add id property (maybe GUID or date int)
    this._value = value?value:""; //item text
    this._collection = collection?collection:null; //reference to Collection where item placed

    //methods
    //validate item
    this._isValid = function(){
        //TODO: use private property (change context)
        return !!(this.value);
    };

    this._remove = function(){
        //TODO: use private property (change context)
        if(this.collection && this.collection.mRemoveItem){
            if(this.collection.mRemoveItem(this)){
                return this.collection;
            }
        }

        console.log("Have no reference to collection or no method to remove item. Can`t remove.")
        return null;
    };

    //public
    return {
        value: this._value,
        collection: this._collection,
        mIsValid: this._isValid,
        mRemove: this._remove
    }
};