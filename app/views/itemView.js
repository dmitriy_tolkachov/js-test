/**
 * Created by Dmitriy T on 11.10.2014.
 */
var ItemView = function(model){
    this.model = model?model:null;
    this.tmpl = '<li><span class="item-name">'+this.model.value+'</span><button title="Remove item from the list" class="remove-item">x</button></li>';
    this.render = function(){
        return this.tmpl;
    }
}