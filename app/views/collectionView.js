/**
 * Created by Dmitriy T on 11.10.2014.
 */
var CollectionView = function(id,collection){
    this.collection = collection?collection:null;
    this.tmpl = '<ul>{{items}}</ul><p><span>{{cnt}}</span> items in the list</p>';

    this.removeItem = function(){
        var itemValueToDelete = this.parentElement.getElementsByClassName('item-name')[0].innerText;
        var item = new Item(itemValueToDelete);
        app.controller.collection.mRemoveItem(item);
        app.controller.views.collection.render();
    }

    //TODO: common properties -> move to separated class
    this.containerId = id?id:""; //element id where to insert the view
    this.render = function(){
        //render items
        var itemsTmpl = "";
        for(var i in this.collection.items){
            var itemView = new ItemView(this.collection.items[i]);
            itemsTmpl += itemView.render();
        }

        // replace
        var tmpl = this.tmpl; //make copy of tmpl to work with
        tmpl = tmpl.replace('{{cnt}}',this.collection.mItemsCount());
        tmpl = tmpl.replace('{{items}}',itemsTmpl);

        //add to page
        document.getElementById(this.containerId).innerHTML = tmpl;
        //add events
        var itemViews = document.getElementsByClassName('remove-item');
        for(var i in itemViews){
            if(typeof(itemViews[i])=='object'){
                itemViews[i].addEventListener("click", this.removeItem);
            }
        }
    }
}