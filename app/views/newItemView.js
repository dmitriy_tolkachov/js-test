/**
 * Created by Dmitriy T on 11.10.2014.
 */
var NewItemView = function(id,collection){
    this.tmpl = '<div><input id="item-value" type="text" maxlength="100" placeholder="Add item to list" autofocus><button title="Add item to the list" id="add-item">Add</button></div>';
    this.add = function(){
        //get value
        var newValue = document.getElementById('item-value').value;
        //if no value - do not add item
        if(!newValue){
            return;
        }
        //create new Item
        var item = new Item(newValue);
        app.controller.collection.mAddItem(item);
        app.controller.views.collection.render();
    }

    //TODO: common properties -> move to separated class
    this.containerId = id?id:""; //element id where to insert the view
    this.render = function(){
        document.getElementById(this.containerId).innerHTML = this.tmpl;
        document.getElementById("add-item").addEventListener("click", this.add); //add event listener
    }
}
