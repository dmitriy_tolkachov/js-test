/**
 * Created by Dmitriy T on 11.10.2014.
 */

//app objects structure
var app = {
    controller: null
};

app.controller = new appController();

//for test
app.controller.addTestsData();

app.controller.init();