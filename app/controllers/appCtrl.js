/**
 * Created by Dmitriy T on 11.10.2014.
 */
var appController = function () {
    //properties
    this.views = {};
    this.collection = new Collection();

    //methods
    //test data
    this.addTestsData = function(){
        this.collection.mAddItem(new Item("Carrots"));
        this.collection.mAddItem(new Item("Pears"));
        this.collection.mAddItem(new Item("Fish"));
        this.collection.mAddItem(new Item("Milk"));
        this.collection.mAddItem(new Item("Bread"));
    };

    this.init = function(){
        this.views.newItem = new NewItemView('new-item',this.collection);
        this.views.newItem.render();

        this.views.collection = new CollectionView('items',this.collection);
        this.views.collection.render();
    };
};